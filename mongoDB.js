/*mysql DB config*/
// var mysql = require('mysql');
// var connection = mysql.createConnection({
//   host     : 'localhost',
//   user     : 'root',
//   password : 'root',
//   database : 'signature'
// });

// connection.connect(function(err) {
//   if (err) {
//     console.error('error connecting: ' + err.stack);
//     return;
//   }
 
//   console.log('connected as id ' + connection.threadId);
// });

// module.exports = connection;

/* mongo DB config*/
var MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';
const dbName = 'signature';
var _db = {};

module.exports = {
	connectToServer: function( callback ) {
		MongoClient.connect(url , { useNewUrlParser: true }, function(err, db){
	      _db = db.db(dbName);
	      return callback( err );
	    } );
	},
	getDb: function(){
		return _db;
	}
};

var express = require('express');
var router = express.Router();
// var DB = require('../mongoDB.js'); // mysql db instance
var mongoDB = require('../mongoDB.js'); //mongo db instance

/* GET users listing. */
// router.get('/', function(req, res, next){
// 	let sql = `CALL getUserList()`;
// 	DB.query(sql, (error, results, fields) => {
// 	  if (error) res.json(error.message);
// 	  res.json(results[0]);
// 	});
// });

// mongo db get api
router.get('/', function(req, res, next){
	var DB = mongoDB.getDb();
	console.log(DB);
	DB.collection('users').find().toArray(function(err, results){
		if (err) res.json(error.message);
		res.json(results[0]);
	})
});


/* POST insert new user*/
// router.post('/register', function(req, res, next){
// 	let sql = `CALL addNewUser(?, ?, ?, ?)`;
// 	DB.query(sql, [req.body.username, req.body.email, req.body.contact, req.body.password], 
// 		(error, results, fields) => {
// 	  if (error) res.json(error.message);
// 	  res.json(results[0]);
// 	});
// });

/*email, password*/
router.post('/register', function(req, res){
	var DB = mongoDB.getDb();
	var dataToQuery = {
		"email": req.body.email
	};
	DB.collection('users').find(dataToQuery).toArray(function(err, result){
	  	if(err) res.send(err);
	  	else {
	  		if(result.length != 0) {
	  			res.json({status:'102', msg:"User already exist."});
	  		} else {
	  			var pass = parseInt( Math.random() * 10000 );
		  		req.body.password = pass.length == 4 ? pass : '0'+pass;

	  			res.mailer.send('sendPass', {
				    to: req.body.email, // REQUIRED. This can be a comma delimited string just like a normal email to field. 
				    subject: 'Signature Register Password', // REQUIRED.
				    user: {
				    	email: req.body.email,
				    	password: req.body.password	
				    }
				}, function (err) {
				    if (err) {
				      console.log(err);
				      res.send('There was an error sending the email');
				      return;
				    }
		  			DB.collection('users').insert(req.body, function(err, result){
					  	if(err) res.json(err);
					  	else res.json(result);
					});
				});
	  		}
	  	}
	});
});

router.post('/generateOTP', function(req, res){
	var DB = mongoDB.getDb();

	var rand = parseInt( Math.random() * 100000 );
	var OTP = rand.length == 6 ? rand : '0'+rand;
	var dataToSave = {
		contact: req.body.contact,
		OTP: OTP
	};
	DB.collection('temp').insertOne(dataToSave, function(err, result){
	  	if(err) res.json(err);
	  	else {
	  		setTimeout(()=> {
	  			var dataToDelete = {
	  				contact: req.body.contact
	  			};
	  			DB.collection('temp').deleteOne(dataToDelete, function(err, result){
	  				if(err) console.log(err);
	  				else console.log("OTP deleted");
	  			});
	  		}, 60000);
	  		res.json({status:200, OTP: OTP});	
	  	}
	});
});

router.post('/verifyOTP', function(req, res){
	var DB = mongoDB.getDb();
	var dataToQuery = {
		contact: req.body.contact
	};
	DB.collection('temp').find(dataToQuery).toArray(function(err, result){
		if(err) res.send(err);
		else {
			if(result.length != 0) {
				if(result[0].OTP == req.body.OTP){
					res.json({status:200, msg: 'Number verified successfully.'});
				} else {
					res.json({status:200, msg: 'You entered a wrong OTP.'});
				}
			} else {
				res.json({status:200, msg: 'OTP was expired, please generate new one'});
			}
		}
	});
});

router.post('/login', function(req, res){
	res.setHeader('Content-Type', 'application/json');
	var DB = mongoDB.getDb();
	var dataToQuery = {
		"email": req.body.email
	};
	DB.collection('users').find(dataToQuery).toArray(function(err, result){
	  	if(err) res.send(err);
	  	else {
	  		if(result.length != 0) {
	  			if(req.body.password == result[0].password){
	  				res.json(result);
	  			} else {
	  				res.json({status:'102', msg:"Password not matched."});	
	  			}
	  		} else {
	  			res.json({status:'102', msg:"User not exist."});
	  		}
	  	}
	});
});

module.exports = router;
